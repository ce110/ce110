#include<stdio.h>
int main()
{
    int m,n;
    printf("enter the value of m(rows) and n(columns): \n");
    scanf("%d%d",&m,&n);
    
    int a[m][n],trans_a[n][m];
	
	printf("enter a matrix\n");
    
    for(int i=0; i<m; i++)
    {
        for(int j=0; j<n; j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
	
	printf("the entered matrix\n");
	for(int i=0; i<m; i++)
    {
        for(int j=0; j<n; j++)
        {
            printf("%d\t",a[i][j]);
        }
        printf("\n");
    }
    
    for(int i=0; i<m; i++)
    {
        for(int j=0; j<n; j++)
        {
            trans_a[j][i]=a[i][j];
        }
    }
	
	printf("the transpose of matrix:\n");
    
    for(int i=0; i<n; i++)
    {
        for(int j=0; j<m; j++)
        {
            printf("%d\t",trans_a[i][j]);
        }
        printf("\n");
    }
    return 0;
    
    
}