#include<stdio.h>

swap(int *x,int *y);

int main()
{
    int a,b;
	printf("enter two numbers\n");
	scanf("%d%d",&a,&b);
	
	swap(&a,&b);
	
	printf("the numbers after swapping\n%d\n%d",a,b);
	
	return 0;
}

swap(int *x,int *y)
{
    *x = *x + *y;
	*y = *x - *y;
	*x = *x - *y;
}