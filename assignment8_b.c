#include<stdio.h>
int binary_search(int a[],int n,int key);
int main()
{
    int key,a[20],i,n,m;
    printf("enter the number which is to be searched: ");
    scanf("%d",&key);

    printf("enter the number of elements: ");
    scanf("%d",&n);

    printf("enter numbers in order(sorted)\n");

    for ( i=0; i<=n-1; i++)
    {
        scanf("%d",&a[i]);
    }

    m=binary_search(a,n,key);

    if(m==-1)
    printf("the number is not in the given array");
    else
    {
        printf("%d is in position %d",key,m+1);
    }
    return 0;
}

int binary_search(int a[],int n,int key)
{
    int low=0;
    int high=n-1;
    int mid;


    while(low<=high)
    {
        mid=(high+low)/2;

        if(key==a[mid])
        return mid;

        if(key<a[mid])
        high=mid-1;

        if(key>a[mid])
        low=mid+1;

    }

    return -1;
}

