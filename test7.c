#include<stdio.h>

struct employee
{
    int eID;
	char ename[20];
	int esalary;
	char equalif[20];
	char eDOJ[20];
};

int main()
{
    struct employee e;
	
	printf("enter the details of employee\n");
	printf("enter employee ID: ");
	scanf("%d",&e.eID);
	printf("enter employee name: ");
	scanf("%s",e.ename);
	printf("enter employee salary: ");
	scanf("%d",&e.esalary);
	printf("enter employee qualification: ");
	scanf("%s",e.equalif);
	printf("enter employee Date of joining: ");
	scanf("%s",e.eDOJ);
	
	printf("The employee details are as follows:\n");
	printf(" employee ID: %d\n",e.eID);
	printf(" employee name: %s\n ",e.ename);
	printf(" employee salary: %d\n",e.esalary);
	printf(" employee qualification: %s\n",e.equalif);
	printf(" employee Date of joining: %s\n",e.eDOJ);
	
	return 0;
}