#include<stdio.h>

int main()
{
    int x,y;
    printf("enter two numbers\n");
    scanf("%d%d",&x,&y);
    
    int *a = &x;
    int *b = &y;
    
    int sum,difference,product,quotient,remainder;
    
    sum = *a + *b;
    difference = *a - *b;
    product = (*a)*(*b);
    quotient = (*a)/(*b);
    remainder = (*a)%(*b);
    
    printf("the sum of two numbers id %d\n",sum);
    printf("the difference of two numbers id %d\n",difference);
    printf("the product of two numbers id %d\n",product);
    printf("the quotient of two numbers id %d\n",quotient);
    printf("the remainder of two numbers id %d\n",remainder);
    
    return 0;
}